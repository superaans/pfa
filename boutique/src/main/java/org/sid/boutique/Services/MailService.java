package org.sid.boutique.Services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;



@Component
public class MailService {

    @Autowired
    JavaMailSender javaMailSender;


    public boolean sendEmail(String email, String code)  {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("BoutiqueMedina@gmail.com");
        message.setTo(email);
        message.setSubject("[BoutiqueMédina] : Verify your email" );
        message.setText("VerifyYourEmail : " + code );
        javaMailSender.send(message);
        return true;
    }
}
