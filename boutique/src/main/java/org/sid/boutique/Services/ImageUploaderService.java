package org.sid.boutique.Services;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Component
public class ImageUploaderService {

    public String Convert(MultipartFile File){
        byte[] bytes = null;
        try {
            bytes = File.getBytes();
            return Base64.encodeBase64String(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
