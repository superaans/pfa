package org.sid.boutique.Services;

import org.sid.boutique.Dao.GerantRepository;
import org.sid.boutique.Entities.Gerant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class GerantService {
    @Autowired
    GerantRepository gerantRepository;

    public Gerant Ajouter(Gerant gerant){
        return gerantRepository.save(gerant);
    }

    public Gerant delete(Gerant gerant){
        gerantRepository.delete(gerant);
        return gerant;
    }

    public Gerant FindGerant(Long id){
        return gerantRepository.findById(id).get();
    }

    public List<Gerant> FindAll(){
        return gerantRepository.findAll();
    }
}
