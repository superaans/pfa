package org.sid.boutique.Services;

import org.sid.boutique.Dao.CommandeRepository;
import org.sid.boutique.Entities.Commande;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommandeService {
    @Autowired
    CommandeRepository commandeRepository;


    public List<Commande> ajouter(List<Commande> commandes){
        return commandeRepository.saveAll(commandes);
    }
    public List<Commande> findByMagasinId(Long id){
        return commandeRepository.findByMagasin_Id(id);
    }

}
