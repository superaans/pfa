package org.sid.boutique.Services;

import org.sid.boutique.Dao.FournisseurRepository;
import org.sid.boutique.Entities.Fournisseur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FournisseurService
{
    @Autowired
    FournisseurRepository fournisseurRepository;

    public List<Fournisseur> find(Long id){
        return fournisseurRepository.findByGerant_Id(id);
    }

    public Fournisseur save(Fournisseur fournisseur){
        return fournisseurRepository.save(fournisseur);
    }

    public Fournisseur get(Long id){
        return fournisseurRepository.findById(id).get();
    }

    public Fournisseur update(Fournisseur fournisseur){
        return fournisseurRepository.save(fournisseur);
    }

    public void delete(Fournisseur fournisseur){
        fournisseurRepository.delete(fournisseur);
    }

}
