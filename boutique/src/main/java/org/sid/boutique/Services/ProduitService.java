package org.sid.boutique.Services;

import org.sid.boutique.Dao.ProduitRepository;
import org.sid.boutique.Entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class ProduitService {

    @Autowired
    public ProduitRepository produitRepository;

    public Produit Ajouter(Produit produit)
    {
        return produitRepository.save(produit);
    }

    public List<Produit> FindProduit(Long MagasinId)
    {
        return produitRepository.findByMagasin_Id(MagasinId);
    }

}
