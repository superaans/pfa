package org.sid.boutique.Services;

import org.sid.boutique.Dao.MagasinRepository;
import org.sid.boutique.Entities.Magasin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MagasinService {

    @Autowired
    MagasinRepository magasinRepository;

    public Magasin Ajouter(Magasin magasin){
        return magasinRepository.save(magasin);
    }

    public List<Magasin> find(Long GerantId){
        return magasinRepository.findByGerant_Id(GerantId);
    }

}
