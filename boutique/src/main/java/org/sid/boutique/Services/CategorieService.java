package org.sid.boutique.Services;

import org.sid.boutique.Dao.CategorieRepository;
import org.sid.boutique.Entities.Categorie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategorieService
{
    @Autowired
    CategorieRepository categorieRepository;

    public Categorie Ajouter(Categorie categorie){
        return categorieRepository.save(categorie);
    }

    public List<Categorie> FindAll(){
        return categorieRepository.findAll();
    }

    public void delete(Long id){
        categorieRepository.deleteById(id);
    }
}
