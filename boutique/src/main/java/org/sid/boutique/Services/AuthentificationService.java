package org.sid.boutique.Services;

import org.sid.boutique.Dao.GerantRepository;
import org.sid.boutique.Entities.Gerant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AuthentificationService {
    @Autowired
    GerantRepository gerantRepository;
    public Gerant Login(String email, String password){

        return gerantRepository.findByEmailAndPassword(email,password);
    }
    public Gerant Inscription(Gerant gerant){
        if(!gerantRepository.existsByEmail(gerant.getEmail())) {

            return gerantRepository.save(gerant);

        }
        return null;

    }
    public Gerant upadate(Gerant gerant){
        return gerantRepository.save(gerant);
    }


}
