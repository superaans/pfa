package org.sid.boutique.Services;

import org.sid.boutique.Dao.FactureRepository;
import org.sid.boutique.Dao.ProduitRepository;
import org.sid.boutique.Entities.Facture;
import org.sid.boutique.Entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FactureService {

    @Autowired
    FactureRepository factureRepository;

     @Autowired
     ProduitRepository produitRepository;

    public Facture ajouter(Facture facture){
        Produit produit = produitRepository.getById(facture.getProduit().getId());
        produit.setQuantite(produit.getQuantite() + facture.getQuantite());
        produitRepository.save(produit);
        return factureRepository.save(facture);
    }

    public List<Facture> findByMagasinId(long magasinId){
        return factureRepository.findByMagasin_Id(magasinId);
    }
}
