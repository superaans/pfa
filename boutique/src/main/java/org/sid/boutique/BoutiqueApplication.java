package org.sid.boutique;

import org.sid.boutique.Entities.*;
import org.sid.boutique.Services.CategorieService;
import org.sid.boutique.Services.GerantService;
import org.sid.boutique.Services.MagasinService;
import org.sid.boutique.Services.ProduitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoutiqueApplication implements CommandLineRunner
{
	@Autowired
	GerantService gerantService;
	@Autowired
	MagasinService magasinService;
	@Autowired
	CategorieService categorieService;
	@Autowired
	ProduitService produitService;
	public static void main(String[] args) {
		SpringApplication.run(BoutiqueApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception
	{
		  Gerant gerant = new Gerant("Oussama", "oussama004156@gmail.com", null,"aans0041",  "0697198155", "Oujda", true);
		  gerantService.Ajouter(gerant);
	    	Gerant gerant1 = new Gerant("ouidad", "benmeryeme_ouidad99@outlook.com", null,"ouidad99",  "0608609628", "Oujda", true);
	    	gerantService.Ajouter(gerant1);
		  	Magasin magasin = new Magasin("test", (String) null, 1000.0,gerant);
			Categorie categorie = new Categorie("vêtement");
			Produit produit = new Produit("designation designation", "U", 1000, 200F, "description", "x", "x", "x", "x",categorie,magasin);

		magasinService.Ajouter(magasin);
		categorieService.Ajouter(categorie);
		produitService.Ajouter(produit);

	}
}
