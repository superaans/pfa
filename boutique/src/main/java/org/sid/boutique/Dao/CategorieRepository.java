package org.sid.boutique.Dao;

import org.sid.boutique.Entities.Categorie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategorieRepository extends JpaRepository<Categorie, Long> {
}