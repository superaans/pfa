package org.sid.boutique.Dao;

import org.sid.boutique.Entities.Magasin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MagasinRepository extends JpaRepository<Magasin, Long> {
    List<Magasin> findByGerant_Id(Long id);

}