package org.sid.boutique.Dao;

import org.sid.boutique.Entities.Facture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FactureRepository extends JpaRepository<Facture, Long>
{
    List<Facture> findByMagasin_Id(Long id);

    List<Facture> findByFournisseur_Id(Long id);




}