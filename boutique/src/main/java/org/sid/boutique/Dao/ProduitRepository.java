package org.sid.boutique.Dao;

import org.sid.boutique.Entities.Produit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProduitRepository extends JpaRepository<Produit, Long> {
    List<Produit> findByMagasin_Id(Long id);


}