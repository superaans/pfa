package org.sid.boutique.Dao;

import org.sid.boutique.Entities.Fournisseur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FournisseurRepository extends JpaRepository<Fournisseur, Long> {
    List<Fournisseur> findByGerant_Id(Long id);
}