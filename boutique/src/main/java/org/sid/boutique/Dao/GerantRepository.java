package org.sid.boutique.Dao;

import org.sid.boutique.Entities.Gerant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface GerantRepository extends JpaRepository<Gerant, Long> {

    @Override
    Optional<Gerant> findById(Long aLong);

    @Query(value = "select * from Gerant  where email = ?1 and password = ?2" , nativeQuery = true)
    Gerant findByEmailAndPassword(String Email, String Password);

    @Query("select (count(*) > 0) from Gerant  where email = ?1")
    boolean existsByEmail(String Email);

}