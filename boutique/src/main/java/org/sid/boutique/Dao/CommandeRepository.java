package org.sid.boutique.Dao;

import org.sid.boutique.Entities.Commande;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommandeRepository extends JpaRepository<Commande, Long> {
    List<Commande> findByMagasin_Id(Long id);
}