package org.sid.boutique.Controller;


import org.sid.boutique.Entities.Commande;
import org.sid.boutique.Entities.Facture;
import org.sid.boutique.Services.FactureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200/")
@RestController
@RequestMapping("/facture")
public class FactureController {

    @Autowired
    FactureService factureService;

    @PostMapping()
    public Facture Ajouter(@RequestBody Facture facture){
        return factureService.ajouter(facture);
    }

    @GetMapping()
    public List<Facture> findAll(@RequestHeader Long magasinId){
        return factureService.findByMagasinId(magasinId);
    }


}
