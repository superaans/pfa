package org.sid.boutique.Controller;


import org.sid.boutique.Services.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("http://localhost:4200/")
@RestController
@RequestMapping("/mail")
public class MailController {

    @Autowired
    MailService mailService;

    @GetMapping
    public String sendEmail(@RequestHeader String email , @RequestHeader String code){
        if(mailService.sendEmail(email, code)){
            return "sended";
        }
        else return "not sended";
    }
}



