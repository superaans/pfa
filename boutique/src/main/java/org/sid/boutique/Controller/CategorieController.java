package org.sid.boutique.Controller;

import org.sid.boutique.Entities.Categorie;
import org.sid.boutique.Entities.Fournisseur;
import org.sid.boutique.Services.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@CrossOrigin("http://localhost:4200/")
@RestController
@RequestMapping("/Categorie")

public class CategorieController
{
    @Autowired
    CategorieService categorieService;

    @GetMapping()
    public List<Categorie> find()
    {
        return categorieService.FindAll();
    }


}
