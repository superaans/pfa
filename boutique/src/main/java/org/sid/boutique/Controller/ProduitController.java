package org.sid.boutique.Controller;
import org.sid.boutique.Dao.ProduitRepository;
import org.sid.boutique.Entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/Produits")
public class ProduitController
{
    @Autowired
    ProduitRepository produitRepository;
    @PostMapping()
    public Produit Ajouter(@RequestBody Produit produit)
    {
        return produitRepository.save(produit);
    }
    @GetMapping()
    public List<Produit> FindAll(@RequestHeader Long MagasinId)
    {
        return produitRepository.findByMagasin_Id(MagasinId);
    }
    @GetMapping("/tfo")
    public List<Produit> getAll()
    {
        return produitRepository.findAll();
    }
    @GetMapping("/{id}")
    public Produit getdetail(@PathVariable Long id)
    {
        return produitRepository.findById(id).get();
    }



}
