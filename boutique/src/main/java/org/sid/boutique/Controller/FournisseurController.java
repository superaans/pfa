package org.sid.boutique.Controller;

import org.sid.boutique.Dao.FournisseurRepository;
import org.sid.boutique.Entities.Fournisseur;
import org.sid.boutique.Services.FournisseurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200/")
@RestController
@RequestMapping("/Fournisseurs")
public class FournisseurController
{
        @Autowired
        FournisseurService fournisseurService;

        @PostMapping()
        public Fournisseur find(@RequestBody Fournisseur fournisseur)
        {
            return fournisseurService.save(fournisseur);
        }
        @GetMapping()
        public List<Fournisseur> find(@RequestHeader Long id)
        {
                return fournisseurService.find(id);
        }

        @DeleteMapping()
        public void delete(@RequestHeader Long id)
        {
                fournisseurService.delete(fournisseurService.get(id));
        }

        @PutMapping()
        public Fournisseur update(@RequestBody Fournisseur fournisseur)
        {
                return fournisseurService.save(fournisseur);
        }
}
