package org.sid.boutique.Controller;

import org.sid.boutique.Services.ImageUploaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin("http://localhost:4200")
@RestController
@RequestMapping("image")
public class ImageUploaderController {

    @Autowired
    ImageUploaderService imageUploderService;
    @PostMapping
    public String convert(@RequestBody MultipartFile file){
        return imageUploderService.Convert(file);
    }
}
