package org.sid.boutique.Controller;

import org.sid.boutique.Entities.Commande;
import org.sid.boutique.Services.CommandeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200/")
@RestController
@RequestMapping("/commande")
public class CommandeController {

    @Autowired
    CommandeService commandeService;

    @PostMapping()
    public List<Commande> Ajouter(@RequestBody List<Commande> commandes){
        System.out.println(commandes);
        return commandeService.ajouter(commandes);
    }

    @GetMapping()
    public List<Commande> findAll(@RequestHeader Long magasinId){
        return commandeService.findByMagasinId(magasinId);
    }
    
}
