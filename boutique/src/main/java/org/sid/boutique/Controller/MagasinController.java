package org.sid.boutique.Controller;

import org.sid.boutique.Entities.Magasin;
import org.sid.boutique.Services.MagasinService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("http://localhost:4200/")
@RestController
@RequestMapping("/magasins")
public class MagasinController
{

    @Autowired
    MagasinService magasinService;

    @GetMapping()
    public List<Magasin> find(@RequestHeader Long id){
        return magasinService.find(id);
    }

    @PostMapping()
    public Magasin ajouter(@RequestBody Magasin magasin){
        return magasinService.Ajouter(magasin);
    }





}
