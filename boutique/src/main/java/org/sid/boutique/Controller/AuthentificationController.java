package org.sid.boutique.Controller;

import org.sid.boutique.Entities.Gerant;
import org.sid.boutique.Services.AuthentificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("http://localhost:4200/")
@RestController
@RequestMapping("/authentification")
public class AuthentificationController {

    @Autowired
    AuthentificationService authentificationService;

    @GetMapping()
    public Gerant login(@RequestHeader String email ,@RequestHeader  String password){
        return authentificationService.Login(email,password);
    }

    @PostMapping()
    public Gerant inscription(@RequestBody Gerant gerant){
        return authentificationService.Inscription(gerant);
    }

    @PutMapping()
    public Gerant update(@RequestBody Gerant gerant){
        return authentificationService.upadate(gerant);
    }
}
