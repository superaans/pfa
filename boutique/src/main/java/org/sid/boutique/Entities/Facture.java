package org.sid.boutique.Entities;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;


@Getter
@Setter
@Entity
@Table(name = "facture")
@NoArgsConstructor
public class Facture {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private Date DateCreation;
    private Double Prix;
    private int Quantite;
    @ManyToOne
    @JoinColumn(name = "fournisseur_id")
    private Fournisseur fournisseur;

    @ManyToOne
    @JoinColumn(name = "magasin_id")
    private Magasin magasin;

    @ManyToOne
    @JoinColumn(name = "produit_id")
    @JsonIgnoreProperties(value = {"factures"})
    private Produit produit;

    public Facture(Date dateCreation, Double prix, int quantite, Fournisseur fournisseur, Magasin magasin, Produit produit) {
        DateCreation = dateCreation;
        Prix = prix;
        Quantite = quantite;
        this.fournisseur = fournisseur;
        this.magasin = magasin;
        this.produit = produit;
    }
}