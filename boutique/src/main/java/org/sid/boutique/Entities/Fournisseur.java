package org.sid.boutique.Entities;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "fournisseur")
public class Fournisseur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String Nom;
    private String Adresse;
    private String Telephone;
    private String Ville;

    @ManyToOne
    @JoinColumn(name = "gerant_id")
    private Gerant gerant;

    public Fournisseur(String nom, String adresse, String telephone, String ville,Gerant gerant)
    {
        Nom = nom;
        Adresse = adresse;
        Telephone = telephone;
        Ville = ville;
        this.gerant=gerant;
    }

}