package org.sid.boutique.Entities;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "magasin")
public class Magasin {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String Nom;
    @Column(length = 65000)
    private String Logo;
    private Double Cf;

    @ManyToOne
    @JoinColumn(name = "gerant_id")
    @JsonIgnoreProperties(value = {"magasin"})
    private Gerant gerant;

    @OneToMany(mappedBy = "magasin", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"magasin"})
    private List<Produit> produits = new ArrayList<>();


    public Magasin(String nom, String logo, Double Cf , Gerant gerant) {
        Nom = nom;
        Logo = logo;
        this.Cf = Cf;
        this.gerant = gerant;
    }

}