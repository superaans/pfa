package org.sid.boutique.Entities.Useful;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ImageModel
{

    private byte[] Data;
    private String Path;

    public ImageModel(byte[] data, String path) {
        Data = data;
        Path = path;
    }
}
