package org.sid.boutique.Entities;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;
import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "gerant")
public class Gerant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String Nom;
    private String Email;
    private Date DateNaissance;
    private String Password;
    private String Telephone;
    private String Ville;

    private Boolean Type;

    public Gerant(String nom, String email, Date dateNaissance, String password, String telephone, String ville, Boolean Type) {
        Nom = nom;
        Email = email;
        DateNaissance = dateNaissance;
        Password = password;
        Telephone = telephone;
        Ville = ville;
        this.Type = Type;
    }
}