package org.sid.boutique.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "produit")

public class Produit
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private String Designation;
    private String Unite;
    private Integer Quantite;
    private Float Prix;
    private String Description;
    @Column(length = 65000)
    private String Image1;
    @Column(length = 65000)
    private String Image2;
    @Column(length = 65000)
    private String Image3;
    @Column(length = 65000)
    private String Image4;
    @ManyToOne
    @JoinColumn(name = "categorie_id")
    private Categorie categorie;
    @ManyToOne
    @JoinColumn(name = "magasin_id")
    @JsonIgnoreProperties(value = {"produits"})
    private Magasin magasin;

    @OneToMany(mappedBy = "produit", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"produit" , "magasin"})
    private List<Facture> factures = new ArrayList<>();


    public Produit(String designation, String unite, Integer quantite, Float prix, String description, String image1, String image2, String image3, String image4, Categorie categorie, Magasin magasin) {
        Designation = designation;
        Unite = unite;
        Quantite = quantite;
        Prix = prix;
        Description = description;
        Image1 = image1;
        Image2 = image2;
        Image3 = image3;
        Image4 = image4;
        this.categorie = categorie;
        this.magasin = magasin;
    }

}
