package org.sid.boutique.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "commande")

public class Commande {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    private Date DateCreation;
    private Float Montant;
    private String Etat;
    private int Qte;

    @ManyToOne
    @JoinColumn(name = "magasin_id")
    private Magasin magasin;

    @ManyToOne
    @JoinColumn(name = "produit_id")
    private Produit produit;

    @ManyToOne
    @JoinColumn(name = "gerant_id")
    private Gerant gerant;

    public Commande(Date dateCreation, Float montant, String etat, int qte, Magasin magasin, Produit produit, Gerant gerant) {
        DateCreation = dateCreation;
        Montant = montant;
        Etat = etat;
        Qte = qte;
        this.magasin = magasin;
        this.produit = produit;
        this.gerant = gerant;
    }
}