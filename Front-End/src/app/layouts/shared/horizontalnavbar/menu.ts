import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
    
    {
        id: 1,
        label: 'Home',
        icon: ' ri-home-3-fill',
        link: '/'
    },
    {
        id: 58,
        label: 'Cart',
        icon: 'ri-shopping-cart-2-fill',
        link: '/cart'
    },
    {
        id: 21,
        label: 'Devenir un Gérant',
        icon: ' ri-account-circle-fill',
        link: '/dashboard'
       
    },
];

