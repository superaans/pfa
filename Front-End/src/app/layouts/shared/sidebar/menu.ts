import { MenuItem } from './menu.model';

export const MENU: MenuItem[] = [
    /*{
        id: 1,
        label: 'MENUITEMS.MENU.TEXT',
        isTitle: true
    },*/
    {
        id: 2,
        label: 'MENUITEMS.DASHBOARDS.TEXT',
        icon: 'ri-dashboard-line',
        link: '/dashboard'
    },
    {
        id: 2,
        label: 'Produit',
        icon: 'fas fa-shopping-bag',
      
        link: '/dashboard/produits'
    },
    {
        id: 2,
        label: 'Ajouter Produit',
        icon: 'far fa-plus-square',
      
        link: '/dashboard/ajouterProduit'
    },
    
    {
        id: 2,
        label: 'Commande',
        icon: ' ri-file-list-3-line',

        link: '/dashboard/commande'
    },
    

    {
        id: 2,
        label: 'Shop',
        icon : 'fas fa-store-alt',
        link: '/dashboard/magasin'
    },
    {
        id: 2,
        label: 'Fournisseur',
        icon: 'ri-truck-line',
        link: '/dashboard/fournisseur'
    },

   /* {
        id: 5,
        label: 'MENUITEMS.ECOMMERCE.TEXT',
        icon: 'ri-store-2-line',
        subItems: [


            {
                id: 11,
                label: 'MENUITEMS.ECOMMERCE.LIST.CHECKOUT',
                link: '/ecommerce/checkout',
                parentId: 5
            },
            {
                id: 12,
                label: 'MENUITEMS.ECOMMERCE.LIST.SHOPS',
                link: '/ecommerce/shops',
                parentId: 5
            },
            {
                id: 13,
                label: 'MENUITEMS.ECOMMERCE.LIST.ADDPRODUCT',
                link: '/ecommerce/add-product',
                parentId: 5
            },
            
        ]
    },*/
   
];
