import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagetitleComponent } from './pagetitle/pagetitle.component';
import { ButtonComponent } from './button/button.component';

@NgModule({
  declarations: [PagetitleComponent, ButtonComponent],
  imports: [
    CommonModule
  ],
  exports: [PagetitleComponent , ButtonComponent]
})
export class UiModule { }
