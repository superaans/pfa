import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  public disable = false;
  @Input() class: string;
  @Input() content: string;



  @Output("function") parentFun: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit(): void {
  }

  function() {
    this.disable = true; 
    this.parentFun.emit();
    }
}
