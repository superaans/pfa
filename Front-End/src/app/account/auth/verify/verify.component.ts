import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Gerant } from 'src/Modele/Gerant';
import { AuthentificationService } from 'src/Service/Authentification/authentification.service';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import { MailService } from 'src/Service/Mail/mail.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {

  verifyForm: FormGroup;
  verify = false;
  submitted = false;
  error = '';
  gerant : Gerant;
  code : string;

  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private authentificationService :AuthentificationService,
    private redirectService : RedirectService,
    private gerantService: GerantService,
    private mailService : MailService,

    ) { }

  ngOnInit() {
    this.redirectService.AuthetificationRedirect();
    document.body.removeAttribute('data-layout');
    document.body.classList.add('auth-body-bg');

    this.verifyForm = this.formBuilder.group({
      code: ['', [Validators.required,]],
        });
  }

  // convenience getter for easy access to form fields
  get f() { return this.verifyForm.controls; }

  /**
   * Form submit
   */
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid

    if (this.verifyForm.invalid) {
      return;
    } else {
      if(this.code == this.f.code.value){
        this.gerantService.gerant.type = true;
        this.authentificationService.Modifier( this.gerantService.gerant).subscribe(respond => {
          localStorage.setItem("gerant" ,JSON.stringify(respond));
          this.router.navigate(["/"]);
        })
      }
      }
    }


  sendEmail(){
    this.verify = true;
    this.code =  Math.floor(1000 + Math.random() * 9000).toString();
    console.log(this.code);
    this.mailService.sendMail(this.gerantService.gerant.email , this.code)
    .subscribe(respond =>{
      console.log(respond);
    })
  }
}
