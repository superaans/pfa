import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';



import { Gerant } from 'src/Modele/Gerant';
import { AuthentificationService } from 'src/Service/Authentification/authentification.service';
import { GerantService } from 'src/Service/Gerant/gerant.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted = false;
  error = '';
  gerant : Gerant;

  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder,
    private router: Router,
    private authentificationService :AuthentificationService,
    private gerantService: GerantService,

    ) { }

  ngOnInit() {
    document.body.removeAttribute('data-layout');
    document.body.classList.add('auth-body-bg');

    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  /**
   * Form submit
   */
  onSubmit() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    } else {
      this.authentificationService.Login(this.f.email.value , this.f.password.value ).subscribe(response => {
        this.gerant = response;
        if(this.gerant != null){
          console.log(this.gerant)
          this.gerantService.gerant = this.gerant
          localStorage.setItem('gerant' ,JSON.stringify(this.gerantService.gerant));
          const url : string = localStorage.getItem("url")
          if(url != null){
            this.router.navigate([url]);
            localStorage.removeItem("url")
          }else{
            this.router.navigate(["/"]);

          }
      } else{
        this.error = 'email or password are incorrect';
      }
      })    
      } 
    }
  }