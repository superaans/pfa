import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from '../../../core/services/auth.service';
import { environment } from '../../../../environments/environment';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import { MailService } from 'src/Service/Mail/mail.service';
import { AuthentificationService } from 'src/Service/Authentification/authentification.service';

@Component({
  selector: 'app-passwordreset',
  templateUrl: './passwordreset.component.html',
  styleUrls: ['./passwordreset.component.scss']
})

/**
 * Reset-password component
 */
export class PasswordresetComponent implements OnInit, AfterViewInit {

  resetForm: FormGroup;
  submitted = false;
  emailSent = false;
  verify = false;
  error = '';
  success = '';
  loading = false;

  // set the currenr year
  year: number = new Date().getFullYear();
  code: string;


  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router, 
    private mailService : MailService,
    private authentificationService : AuthentificationService,
    ) { }

  ngOnInit() {
    document.body.removeAttribute('data-layout');
    document.body.classList.add('auth-body-bg');

    this.resetForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      code: ['', [Validators.required,]],
      password: ['', [Validators.required,]],
      repeatPassword: ['', [Validators.required,]],


    });
  }

  ngAfterViewInit() {

  }

  // convenience getter for easy access to form fields
  get f() { return this.resetForm.controls; }

  /**
   * On submit form
   */
   changePassword() {
    if(this.f.password.errors == null && this.f.repeatPassword.errors == null && this.f.password.value == this.f.repeatPassword.value){
      
    }
  }

  verifyCode(){
    if(this.f.code.errors == null && this.code == this.f.code.value){
      this.verify = true;
    }
  }

  sendEmail(){
    this.emailSent = true;
    this.code =  Math.floor(1000 + Math.random() * 9000).toString();
    console.log(this.code);
    if(this.f.email.errors == null){
      this.mailService.sendMail(this.f.email.value , this.code)
      .subscribe(respond =>{
        console.log(respond);
      })
    }

  }
}
