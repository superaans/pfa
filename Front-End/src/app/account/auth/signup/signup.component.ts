import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from '../../../core/services/auth.service';
import { environment } from '../../../../environments/environment';
import { first } from 'rxjs/operators';
import { UserProfileService } from '../../../core/services/user.service';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import { Gerant } from 'src/Modele/Gerant';
import { AuthentificationService } from 'src/Service/Authentification/authentification.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, AfterViewInit {

  signupForm: FormGroup;
  submitted = false;
  error = '';
  gerant : Gerant;
  test : Gerant;
  


  // set the currenr year
  year: number = new Date().getFullYear();

  // tslint:disable-next-line: max-line-length
  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, private authentificationService : AuthentificationService) { }

  ngOnInit() {
    document.body.removeAttribute('data-layout');
    document.body.classList.add('auth-body-bg');

    this.signupForm = this.formBuilder.group({
      nom : ['' , Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      telephone : ['' , [Validators.required]],
    });
  }

  ngAfterViewInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  /**
   * On submit form
   */
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.signupForm.invalid) {
      return;
    } else {
      
       this.gerant = {
        id : null,
        nom : this.f.nom.value , 
        email : this.f.email.value,
        password : this.f.password.value,
        telephone : this.f.telephone.value,
        dateNaissance : null ,
        ville :null,
        type : false,
      };

      this.authentificationService.Inscription(this.gerant).subscribe(response => {
        this.test = response;
        if(this.test != null){
          this.router.navigate(['/']);
        } else{
        this.error = 'Email is already taken';
        }
      })
    }
  }
}
