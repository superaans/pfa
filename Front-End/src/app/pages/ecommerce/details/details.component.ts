import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Commande } from 'src/Modele/Commande';
import { Produit } from 'src/Modele/Produit';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import { ProduitsService } from 'src/Service/Produits/produits.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
  public produit : Produit;
  public exist :boolean = false;

  constructor(private router: ActivatedRoute ,
    private produitService: ProduitsService,
    private redirectService :  RedirectService,
    private gerantService : GerantService
      ) { }

  ngOnInit(): void 
  {
    this.getDetails()
  }
  public getDetails()
  {
   
    this.router.params.subscribe(s => {
      const id : string = s["id"];
      console.log(id);

      this.produitService.getdetail(id).subscribe(respond => 
        {
        this.produit = respond;
      })
    })

  }
  successmsg() {
    Swal.fire({
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
    });
  }
  public   commander()
  {
     this.redirectService.AuthetificationRedirect();
     if(this.gerantService.gerant != null && this.gerantService.gerant.type == true){
      const commande : Commande = {
        produit: this.produit,
        etat: "Non confirmer",
        gerant: JSON.parse(localStorage.getItem("gerant")),
        magasin: this.produit.magasin,
        date_creation: new Date(),
        montant: this.produit.prix,
        id: null,
        qte: 1,
      }
      if(localStorage.getItem("cart") == null){
        const commandes : Commande[] = [];
        commandes.push(commande);
        localStorage.setItem("cart" , JSON.stringify(commandes));
        this.exist=false;
        this.successmsg();
        
      }else{
        const commandes : Commande[] = JSON.parse(localStorage.getItem("cart"))
        if(commandes.includes(commande)){
          commandes.push(commande);
          localStorage.setItem("cart" , JSON.stringify(commandes));
          this.successmsg();
          this.exist=false;
        } else{
          this.exist=true;
                }
      }
     }
     


  }
}