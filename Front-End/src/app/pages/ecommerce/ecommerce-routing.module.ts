import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './cart/cart.component';
import { DetailsComponent } from './details/details.component';
import { ProduitComponent } from './produit/produit.component';



const routes: Routes = [
    {
        path: '',
        component: ProduitComponent
    },
    {
        path: 'cart',
        component: CartComponent
    },
    {
        path: 'products/:id',
        component: DetailsComponent
    },



];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EcommerceRoutingModule { }
