import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// tslint:disable-next-line: max-line-length
import { NgbAccordionModule, NgbNavModule, NgbTypeaheadModule, NgbPaginationModule, NgbCollapseModule, NgbTooltipModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ArchwizardModule } from 'angular-archwizard';
import { Ng5SliderModule } from 'ng5-slider';

import { UiModule } from '../../shared/ui/ui.module';


import { EcommerceRoutingModule } from './ecommerce-routing.module';


import { DropzoneModule } from 'ngx-dropzone-wrapper';
import { CartComponent } from './cart/cart.component';
import { ProduitComponent } from './produit/produit.component';
import { DetailsComponent } from './details/details.component';
import { RowComponent } from './cart/row/row.component';


@NgModule({
  // tslint:disable-next-line: max-line-length
  declarations: [
   
    CartComponent,
   
    ProduitComponent,
   
    DetailsComponent,
   
    RowComponent,

   
    ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    EcommerceRoutingModule,
    UiModule,
    ArchwizardModule,
    NgbAccordionModule,
    NgbNavModule,
    NgbTypeaheadModule,
    NgbPaginationModule,
    NgbTooltipModule,
    NgbCollapseModule,
    Ng5SliderModule,
    NgbModalModule,
    DropzoneModule,

  ]
})
export class EcommerceModule { }