import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debounce } from '@fullcalendar/core/util/misc';
import { debounceTime } from 'rxjs/operators';
import { Produit } from 'src/Modele/Produit';
import { ProduitsService } from 'src/Service/Produits/produits.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';

@Component({
  selector: 'app-produit',
  templateUrl: './produit.component.html',
  styleUrls: ['./produit.component.scss']
})
export class ProduitComponent implements OnInit {

  search = new FormControl('');
  constructor(
    private router: Router , 
    private produitService: ProduitsService,
    private redirectService : RedirectService
     ) { }
  public list : Produit[];
  public filtredList : Produit[];
  ngOnInit(): void 
  {
    
    this.getALL();
  }
  public getALL()
  {
    this.produitService.getAll().subscribe(respond => {
      this.list = respond
      this.filtredList = respond
    })
  }

 public filter(){

  this.search.valueChanges.pipe(
    debounceTime(1000)
  ).subscribe(text => {
    if(text == ""){
      console.log(text);
      this.filtredList = this.list;
    }else{
      this.filtredList = this.list.filter(p => p.designation.toLowerCase().includes(text));
      console.log(text);

    }
  });

 }


}
