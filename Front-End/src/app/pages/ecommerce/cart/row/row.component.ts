import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Commande } from 'src/Modele/Commande';

@Component({
  selector: 'app-row',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent implements OnInit {


  quantite = new FormControl('' , [Validators.required]);
  constructor(private formBuilder: FormBuilder) { }

  @Output("parentFun") parentFun: EventEmitter<any> = new EventEmitter();


  @Input() commande :Commande;
  ngOnInit(): void {

    this.quantite.setValue(this.commande.qte);
  }

  public change(){
    if(Number(this.quantite.value) > 0 && Number(this.quantite.value) <= this.commande.produit.quantite){
      this.commande.qte = this.quantite.value;
    }else if (this.quantite.value == "0"){
      this.quantite.setValue("1");
    }else{
      this.quantite.setValue(this.commande.produit.quantite.toString());
    }

    this.parentFun.emit();
    }

    
  }


