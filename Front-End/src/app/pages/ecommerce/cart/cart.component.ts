import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Commande } from 'src/Modele/Commande';
import { Produit } from 'src/Modele/Produit';
import { CommandeService } from 'src/Service/Commande/commande.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';
import Swal from 'sweetalert2';

import { Cart } from './cart.model';

import { cartData } from './data';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
/**
 * Ecommerce Cart component
 */
export class CartComponent implements OnInit {

  // breadcrumb items
  breadCrumbItems: Array<{}>;

  produits : Produit[];
  commandes : Commande[];
  total : number ;

  constructor(private commandeService : CommandeService, private redirectService :  RedirectService) { }

  ngOnInit(): void {
    this.redirectService.AuthetificationRedirect();
    this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Cart', active: true }];
    this.commandes = JSON.parse(localStorage.getItem('cart'))

  }

  successmsg() {
    Swal.fire({
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
    });
  }


  calculerTotal(){
    this.total = 0;
    this.commandes.forEach(function (commande) {
      this.total = this.total + commande.qte * commande.produit.prix;
      console.log(this.total)
  });
  }

  commander(){
     this.commandeService.Ajouter(this.commandes).subscribe(respond => {
       if(respond != null){
        localStorage.removeItem("cart");
        this.commandes = null;
        this.successmsg();
       }
     })
  }


}
