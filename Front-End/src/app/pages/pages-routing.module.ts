import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerticalComponent } from '../layouts/vertical/vertical.component';

import { DashboardComponent } from './dashboard/dashboard.component';



const routes: Routes = [
    { path: '', loadChildren: () => import('./ecommerce/ecommerce.module').then(m => m.EcommerceModule) },

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
