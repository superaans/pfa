import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrationRoutingModule } from './administration-routing.module';
import { CommandeComponent } from './commande/commande.component';
import { FournisseurComponent } from './fournisseur/fournisseur.component';
import { AjouterFournisseurComponent } from './fournisseur/ajouter-fournisseur/ajouter-fournisseur.component';
import { ModifierFournisseurComponent } from './fournisseur/modifier-fournisseur/modifier-fournisseur.component';
import { SuprimerFournisseurComponent } from './fournisseur/suprimer-fournisseur/suprimer-fournisseur.component';
import { ProduitsComponent } from './produits/produits.component';
import { AjouterQuantiteComponent } from './produits/ajouter-quantite/ajouter-quantite.component';
import { DetailsComponent } from './produits/details/details.component';
import { ModifierProduitsComponent } from './produits/modifier-produits/modifier-produits.component';
import { SuprimerProduitsComponent } from './produits/suprimer-produits/suprimer-produits.component';
import { AjouterProduitsComponent } from './ajouter-produits/ajouter-produits.component';
import { MagasinComponent } from './magasin/magasin.component';
import { AjouterMagasinComponent } from './magasin/ajouter-magasin/ajouter-magasin.component';
import {  DropzoneConfigInterface, DropzoneModule, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { NgbAccordionModule, NgbCollapseModule, NgbDropdownModule, NgbModalModule, NgbNavModule, NgbPaginationModule, NgbTooltipModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
import { ArchwizardModule } from 'angular-archwizard';
import { Ng5SliderModule } from 'ng5-slider';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';


const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
  url: 'http://localhost:8080/image',
};
@NgModule({
  declarations: [FournisseurComponent, AjouterFournisseurComponent, ModifierFournisseurComponent, SuprimerFournisseurComponent, ProduitsComponent, AjouterQuantiteComponent, DetailsComponent, ModifierProduitsComponent, SuprimerProduitsComponent, AjouterProduitsComponent, CommandeComponent, MagasinComponent, AjouterMagasinComponent, DashboardComponent],
  imports: [
    CommonModule,
    AdministrationRoutingModule,
    DropzoneModule,
    NgbNavModule,
    NgbDropdownModule,
    ArchwizardModule,
    NgbAccordionModule,
    NgbTypeaheadModule,
    NgbPaginationModule,
    NgbTooltipModule,
    NgbCollapseModule,
    Ng5SliderModule,
    NgbModalModule,
    ReactiveFormsModule,
    FormsModule,
    
  ],

  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }
  ]
})
export class AdministrationModule { }
