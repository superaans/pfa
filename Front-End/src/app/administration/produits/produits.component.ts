import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Produit } from 'src/Modele/Produit';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import { MagasinService } from 'src/Service/Magasin/magasin.service';
import { ProduitsService } from 'src/Service/Produits/produits.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';

@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.scss']
})
export class ProduitsComponent implements OnInit {


  // breadcrumb items
  breadCrumbItems: Array<{}>;
  router : Router;

  list: Produit[];

  constructor(
    private modalService: NgbModal,
    private gerantService : GerantService,
    private produitsService:ProduitsService,
    public magasinService:MagasinService,
    private redirectService : RedirectService,

    )
     { }

  ngOnInit(): void {
        this.redirectService.AuthetificationRedirect();
        this.redirectService.MagasinRedirect();
        this.breadCrumbItems = [{ label: this.magasinService.magasin.nom }, { label: 'Produits', active: true }];
        this.fetchData();
        

  }

  public fetchData() {
    console.log(this.magasinService.magasin);
    this.produitsService.FindAll(this.magasinService.magasin.id).subscribe(response=>{
      this.list = response;
    })
  }
}
