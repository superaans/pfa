import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Facture } from 'src/Modele/Facture';
import { Fournisseur } from 'src/Modele/Fournisseur';
import { Produit } from 'src/Modele/Produit';
import { FactureService } from 'src/Service/Facture/facture.service';
import { FournisseurService } from 'src/Service/Fournisseur/fournisseur.service';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ajouter-quantite',
  templateUrl: './ajouter-quantite.component.html',
  styleUrls: ['./ajouter-quantite.component.scss']
})
export class AjouterQuantiteComponent implements OnInit {
  
  validationform: FormGroup; // bootstrap validation form
  submitted: boolean;
  fournisseur : Fournisseur;
  fournisseurs : Fournisseur[];
  @Output("parentFun") parentFun: EventEmitter<any> = new EventEmitter();
  @Input() produit : Produit;


  constructor(
    public formBuilder: FormBuilder,
    private modalService: NgbModal,
    private gerantService : GerantService,
    private fournisseurService : FournisseurService,
    private factureService :FactureService

    ) { }
  // bread crumb items
  breadCrumbItems: Array<{}>;
  router :Router;


  ngOnInit() {

      this.breadCrumbItems = [{ label: 'Forms' }, { label: 'Form Validation', active: true }];

      console.log(this.produit)
       this.validationform = this.formBuilder.group({
        quantite: ['', [Validators.required]],
        prix: ['', [Validators.required]],
        fournisseur: ['', [Validators.required]],
      });
      this.fournisseurService.FindAll(this.gerantService.gerant).subscribe(respond => {
        this.fournisseurs = respond
      })

  }
  
     validSubmit() {
      this.submitted = true;
      if (this.validationform.invalid)
       {

        return;
      } 
      else
       {
        const fournisseur :Fournisseur = {
          id: this.form.fournisseur.value,
          nom: '',
          adresse: '',
          telephone: undefined,
          ville: undefined,
          gerant: undefined
        }
        const facture : Facture = {
          id: null,
          dateCreation: new Date(),
          prix: this.form.prix.value,
          quantite: this.form.quantite.value,
          magasin: this.produit.magasin,
          produit: this.produit,
          fournisseur: fournisseur
        }

        this.factureService.Ajouter(facture).subscribe(respond => {
          this.successmsg();
          this.produit.quantite =  this.produit.quantite + Number(this.form.quantite.value);
          this.modalService.dismissAll();
        })
      }
    }
    successmsg() {
      Swal.fire({
        icon: 'success',
        title: 'Your work has been saved',
        showConfirmButton: false,
        timer: 1500
      });
    }


      /**
   * Returns form
   */
  get form() {
    return this.validationform.controls;
  }

  /**
   * Modal Open
   * @param content modal content
   */
   openModal(content: any) {
    this.modalService.open(content, { centered: true });
  }



}
