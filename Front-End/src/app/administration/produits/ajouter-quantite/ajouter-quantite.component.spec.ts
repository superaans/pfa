import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterQuantiteComponent } from './ajouter-quantite.component';

describe('AjouterQuantiteComponent', () => {
  let component: AjouterQuantiteComponent;
  let fixture: ComponentFixture<AjouterQuantiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterQuantiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterQuantiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
