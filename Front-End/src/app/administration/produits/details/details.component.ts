import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Produit } from 'src/Modele/Produit';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {


  @Input() produit : Produit;

  constructor(
    private modalService: NgbModal,

  ) { }

  ngOnInit(): void {
  }

  /**
   * Open extra large modal
   * @param exlargeModal extra large modal data
   */
   extraLarge(exlargeModal: any) {
    this.modalService.open(exlargeModal, { size: 'xl' });
  }

}
