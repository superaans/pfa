import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuprimerProduitsComponent } from './suprimer-produits.component';

describe('SuprimerProduitsComponent', () => {
  let component: SuprimerProduitsComponent;
  let fixture: ComponentFixture<SuprimerProduitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuprimerProduitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuprimerProduitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
