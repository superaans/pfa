import { Component, OnInit } from '@angular/core';
import { Commande } from 'src/Modele/Commande';
import { Magasin } from 'src/Modele/magasin';
import { CommandeService } from 'src/Service/Commande/commande.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';
@Component({
  selector: 'app-commande',
  templateUrl: './commande.component.html',
  styleUrls: ['./commande.component.scss']
})
export class CommandeComponent implements OnInit {
msg:string;
  constructor(
    private commandeService : CommandeService,
    private redirectService : RedirectService,
    ) { }

  public commandes : Commande[];
  ngOnInit(): void {
    this.redirectService.AuthetificationRedirect();
    this.redirectService.MagasinRedirect();
    const magasin : Magasin = JSON.parse(localStorage.getItem("magasin"));
    this.commandeService.findAll(magasin.id).subscribe(respond => {
      this.commandes = respond;
    });
  }
  currentDate = new Date();
  
  clickEvent(){
    this.msg='Button is Clicked';
    return this.msg;
}
}
