import { Component, OnInit } from '@angular/core';


import { GerantService } from 'src/Service/Gerant/gerant.service';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { MagasinService } from 'src/Service/Magasin/magasin.service';
import { Magasin } from 'src/Modele/magasin';
import { RedirectService } from 'src/Service/Redirect/redirect.service';

@Component({
  selector: 'app-magasin',
  templateUrl: './magasin.component.html',
  styleUrls: ['./magasin.component.scss']
})
export class MagasinComponent implements OnInit {

 // breadcrumb items
 breadCrumbItems: Array<{}>;

 magasins: Magasin[];
 loading : boolean = true
 
 redirect = null;

 constructor(
   public gerantService : GerantService,
   private magasinService:MagasinService,
   private redirectService : RedirectService,
   private route: ActivatedRoute,
   private router : Router

 ) { }
 
 ngOnInit(): void {

  this.redirectService.AuthetificationRedirect();
  this.breadCrumbItems = [{ label: 'Ecommerce' }, { label: 'Shops', active: true }];
  this.magasinService.Liste(this.gerantService.gerant.id).subscribe(response => {
  this.magasins = response;
  this.loading = false
  this.redirect = this.route.snapshot.paramMap.get("redirect")
     });
 }


 refresh(){
   this.loading= true;
   this.magasinService.Liste(this.gerantService.gerant.id).subscribe(response => {
     this.magasins = response;
     this.loading = false
   })      
 }
 chose(magasin:Magasin){
   this.magasinService.magasin=magasin;
   localStorage.setItem('magasin' ,JSON.stringify(magasin));
   this.router.navigate(['/dashboard']);
 }

}
