import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AjouterMagasinComponent } from './ajouter-magasin.component';

describe('AjouterMagasinComponent', () => {
  let component: AjouterMagasinComponent;
  let fixture: ComponentFixture<AjouterMagasinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AjouterMagasinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AjouterMagasinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
