import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Magasin } from 'src/Modele/magasin';
import { Url } from 'src/Modele/Url';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import { MagasinService } from 'src/Service/Magasin/magasin.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ajouter-magasin',
  templateUrl: './ajouter-magasin.component.html',
  styleUrls: ['./ajouter-magasin.component.scss']
})
export class AjouterMagasinComponent implements OnInit {

  validationform: FormGroup; // bootstrap validation form
  selectedFile: File;
  submitted: boolean;
  magasin : Magasin;
  path : string;

  @Output("parentFun") parentFun: EventEmitter<any> = new EventEmitter();

  constructor(
    public formBuilder: FormBuilder,
    private modalService: NgbModal,
    private magasinService : MagasinService,
    private gerantService : GerantService,
    ) { }
  // bread crumb items
  breadCrumbItems: Array<{}>;



  ngOnInit() {

    this.breadCrumbItems = [{ label: 'Forms' }, { label: 'Form Validation', active: true }];

    /**
     * Bootstrap validation form data
     */
     this.validationform = this.formBuilder.group({
      nom: ['', [Validators.required]],
        });

  }

  successmsg() {
    Swal.fire({
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
    });
  }

    /**
   * Bootsrap validation form submit method
   */
     validSubmit() {
      this.submitted = true;
      if (this.validationform.invalid)
       {
        return;
      } 
      else
       {
            this.magasin = {
              id : null,
              nom : this.form.nom.value,
              logo : this.path,
              cf : 10000,
              gerant : this.gerantService.gerant ,
              produits:null
            };
            this.magasinService.Ajouter(this.magasin).subscribe(respond => {
              if(respond != null){
                this.successmsg();
                this.parentFun.emit();
                this.modalService.dismissAll();
                this.form.nom.setValue("");
              }
            });

      }
    }
    OnFileChange(event){
      this.path = event[0].dataURL;
    }

  get form() {
    return this.validationform.controls;
  }
  
  /**
   * Open extra large modal
   * @param exlargeModal extra large modal data
   */
   extraLarge(exlargeModal: any) {
    this.modalService.open(exlargeModal, { size: 'xl' });
  }

}
