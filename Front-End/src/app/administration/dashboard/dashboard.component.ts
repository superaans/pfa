import { Component, OnInit } from '@angular/core';
import { RedirectService } from 'src/Service/Redirect/redirect.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private redirectService : RedirectService) { }

  ngOnInit(): void {
    this.redirectService.AuthetificationRedirect();
    this.redirectService.MagasinRedirect();
  }

}
