import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Fournisseur } from 'src/Modele/Fournisseur';
import { FournisseurService } from 'src/Service/Fournisseur/fournisseur.service';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';

@Component({
  selector: 'app-fournisseur',
  templateUrl: './fournisseur.component.html',
  styleUrls: ['./fournisseur.component.scss']
})
export class FournisseurComponent implements OnInit {


  // breadcrumb items
  breadCrumbItems: Array<{}>;
  router : Router;

  list: Fournisseur[];

  constructor(
    private modalService: NgbModal,
    private gerantService : GerantService,
    private fournisseurService : FournisseurService,
    private redirectServie : RedirectService
    
    ) { }

  ngOnInit(): void {
    this.redirectServie.AuthetificationRedirect();
      this.breadCrumbItems = [{ label: 'Dashboard' }, { label: 'Fournisseur', active: true }];
      this.fetchData();

  }

  public fetchData() {
    this.fournisseurService.FindAll(this.gerantService.gerant).subscribe(response => {
      this.list = response
    });
  }

 

}
