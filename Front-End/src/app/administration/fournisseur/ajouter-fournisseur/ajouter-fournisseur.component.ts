import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Fournisseur } from 'src/Modele/Fournisseur';
import { FournisseurService } from 'src/Service/Fournisseur/fournisseur.service';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ajouter-fournisseur',
  templateUrl: './ajouter-fournisseur.component.html',
  styleUrls: ['./ajouter-fournisseur.component.scss']
})
export class AjouterFournisseurComponent implements OnInit {

  validationform: FormGroup; // bootstrap validation form
  // Form submition
  submitted: boolean;
  fournisseur : Fournisseur;
  @Output("parentFun") parentFun: EventEmitter<any> = new EventEmitter();

  constructor(
    public formBuilder: FormBuilder,
    private modalService: NgbModal,
    private gerantService : GerantService,
    private fournisseurService : FournisseurService

    ) { }
  // bread crumb items
  breadCrumbItems: Array<{}>;
  router :Router;


  ngOnInit() {
      this.breadCrumbItems = [{ label: 'Forms' }, { label: 'Form Validation', active: true }];
      /**
       * Bootstrap validation form data
       */
       this.validationform = this.formBuilder.group({
        nom: ['', [Validators.required]],
        adresse: ['', [Validators.required]],
        telephone: ['', [Validators.required]],
        ville: ['', [Validators.required, ]],
      });


  }

    /**
   * Bootsrap validation form submit method
   */
     validSubmit() {
      this.submitted = true;
      if (this.validationform.invalid)
       {
        return;
      } 
      else
       {
        this.fournisseur =
         {
          id : null,
          nom : this.form.nom.value , 
          adresse: this.form.adresse.value,
          telephone : this.form.telephone.value,
          ville : this.form.ville.value,
          gerant: this.gerantService.gerant

        }
        this.fournisseurService.Ajouter(this.fournisseur).subscribe(response =>{
          if(response!=null){
            this.parentFun.emit();
            this.successmsg();
            this.submitted = false;
            this.form.nom.setValue(""); 
            this.form.adresse.setValue("");
            this.form.telephone.setValue("");
            this.form.ville.setValue("");
                    }
        })
      }
    }
    successmsg() {
      Swal.fire({
        icon: 'success',
        title: 'Your work has been saved',
        showConfirmButton: false,
        timer: 1500
      });
    }


      /**
   * Returns form
   */
  get form() {
    return this.validationform.controls;
  }

  /**
   * Modal Open
   * @param content modal content
   */
   openModal(content: any) {
    this.modalService.open(content, { centered: true });
  }
       /**
   * save the contacts data
   */
  saveData() {

    if (this.validationform.valid) {
      this.modalService.dismissAll();
    }
    this.submitted = true;
  }


}
