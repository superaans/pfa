import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Fournisseur } from 'src/Modele/Fournisseur';
import { FournisseurService } from 'src/Service/Fournisseur/fournisseur.service';
import { GerantService } from 'src/Service/Gerant/gerant.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-modifier-fournisseur',
  templateUrl: './modifier-fournisseur.component.html',
  styleUrls: ['./modifier-fournisseur.component.scss']
})
export class ModifierFournisseurComponent implements OnInit {

  validationform: FormGroup; // bootstrap validation form
  
  @Input() fournisseur : Fournisseur;
  // Form submition
  submitted: boolean;


  constructor(
    public formBuilder: FormBuilder,
    private modalService: NgbModal,
    private fournisseurService: FournisseurService,
    private gerantService : GerantService

    ) { }
  // bread crumb items
  breadCrumbItems: Array<{}>;



  ngOnInit() {

    this.breadCrumbItems = [{ label: 'Forms' }, { label: 'Form Validation', active: true }];

    /**
     * Bootstrap validation form data
     */
     this.validationform = this.formBuilder.group({
      nom: ['', [Validators.required]],
      adresse: ['', [Validators.required]],
      telephone: ['', [Validators.required]],
      ville: ['', [Validators.required, ]],
    });

    this.form.nom.setValue(this.fournisseur.nom);
    this.form.adresse.setValue(this.fournisseur.adresse);
    this.form.telephone.setValue(this.fournisseur.telephone);
    this.form.ville.setValue(this.fournisseur.ville);

  }

    /**
   * Bootsrap validation form submit method
   */
     validSubmit() {
      this.submitted = true;
      if (this.validationform.invalid)
       {
        return;
      } 
      else
       {
         this.fournisseur.nom = this.form.nom.value
         this.fournisseur.adresse = this.form.adresse.value
         this.fournisseur.telephone = this.form.telephone.value
         this.fournisseur.ville = this.form.ville.value

        this.fournisseurService.Update(this.fournisseur).subscribe(response =>{
          if(response!=null){
            this.successmsg();
          }
        })
      }
    }

    successmsg() {
      Swal.fire({
        icon: 'success',
        title: 'Your work has been saved',
        showConfirmButton: false,
        timer: 1500
      });
    }
      /**
   * Returns form
   */
  get form() {
    return this.validationform.controls;
  }

  /**
   * Modal Open
   * @param content modal content
   */
   openModal(content: any) {
    this.modalService.open(content, { centered: true });
  }
       /**
   * save the contacts data
   */
  saveData() {

    if (this.validationform.valid) {
      this.modalService.dismissAll();
    }
    this.submitted = true;
  }


}
