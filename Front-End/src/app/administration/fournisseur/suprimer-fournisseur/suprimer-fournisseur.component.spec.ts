import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuprimerFournisseurComponent } from './suprimer-fournisseur.component';

describe('SuprimerFournisseurComponent', () => {
  let component: SuprimerFournisseurComponent;
  let fixture: ComponentFixture<SuprimerFournisseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuprimerFournisseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuprimerFournisseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
