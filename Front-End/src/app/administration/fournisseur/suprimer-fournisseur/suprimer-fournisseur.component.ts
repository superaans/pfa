import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Fournisseur } from 'src/Modele/Fournisseur';
import { FournisseurService } from 'src/Service/Fournisseur/fournisseur.service';

import Swal from 'sweetalert2';
@Component({
  selector: 'app-suprimer-fournisseur',
  templateUrl: './suprimer-fournisseur.component.html',
  styleUrls: ['./suprimer-fournisseur.component.scss']
})
export class SuprimerFournisseurComponent implements OnInit {

  // bread crum items
  breadCrumbItems: Array<{}>;

  @Input() id: number;  

  @Output("parentFun") parentFun: EventEmitter<any> = new EventEmitter();
  constructor(
    private fournisseurService: FournisseurService
  ) { }

  ngOnInit() {
    this.breadCrumbItems = [{ label: 'UI Elements' }, { label: 'Sweetalert 2', active: true }];
  }




  successmsg() {
    Swal.fire({
      title: 'Good job!',
      text: 'You clicked the button!',
      icon: 'success',
      showCancelButton: true,
      confirmButtonColor: '#5438dc',
      cancelButtonColor: '#ff3d60'
    });
  }


  confirm() {
    Swal.fire({
      title: 'Are you sure?',
      text: 'You won\'t be able to revert this!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#34c38f',
      cancelButtonColor: '#ff3d60',
      confirmButtonText: 'Yes, delete it!'
    }).then(result => {
      if (result.value) 
      {
        this.fournisseurService.delete(this.id).subscribe();
        console.log('ghedam hadechi')
        Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
      }
    });
  }

  cancel() {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger ml-2'
      },
      buttonsStyling: false
    });

    swalWithBootstrapButtons
      .fire({
        title: 'Are you sure?',
        text: 'You won\'t be able to revert this!',
        icon: 'warning',
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        showCancelButton: true
      })
      .then(result => {
        if (result.value) 
        {
          this.fournisseurService.delete(this.id).subscribe(response => {
            this.parentFun.emit();
            swalWithBootstrapButtons.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );
          })

        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          swalWithBootstrapButtons.fire(
            'Cancelled',
            'Your imaginary file is safe :)',
            'error'
          );
        }
      });
  }
  imageHeader() {
    Swal.fire({
      title: 'Sweet!',
      text: 'Modal with a custom image.',
      imageUrl: 'assets/images/logo-dark.png',
      imageHeight: 20,
      confirmButtonColor: '#5438dc',
      animation: false
    });
  }

  custom() {
    Swal.fire({
      title: '<i>HTML</i> <u>example</u>',
      icon: 'info',
      html:
        'You can use <b>bold text</b>, ' +
        '<a href="//themesdesign.in/">links</a> ' +
        'and other HTML tags',
      showCloseButton: true,
      showCancelButton: true,
      confirmButtonText: '<i class="fa fa-thumbs-up"></i> Great!',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText: '<i class="fa fa-thumbs-down"></i>',
      cancelButtonAriaLabel: 'Thumbs down'
    });
  }


}
