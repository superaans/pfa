import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AjouterProduitsComponent } from './ajouter-produits/ajouter-produits.component';
import { CommandeComponent } from './commande/commande.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FournisseurComponent } from './fournisseur/fournisseur.component';
import { MagasinComponent } from './magasin/magasin.component';
import { ProduitsComponent } from './produits/produits.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'commande', component: CommandeComponent },
  { path: 'fournisseur', component: FournisseurComponent },
  { path: 'produits', component: ProduitsComponent },
  { path: 'ajouterProduit', component: AjouterProduitsComponent },
  { path: 'commande', component: CommandeComponent },
  { path: 'magasin', component: MagasinComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrationRoutingModule { }
