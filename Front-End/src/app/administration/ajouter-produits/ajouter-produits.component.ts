import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Categorie } from 'src/Modele/Categories';
import { Produit } from 'src/Modele/Produit';
import { CategorieService } from 'src/Service/Categorie/categorie.service';
import { MagasinService } from 'src/Service/Magasin/magasin.service';
import { ProduitsService } from 'src/Service/Produits/produits.service';
import { RedirectService } from 'src/Service/Redirect/redirect.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ajouter-produits',
  templateUrl: './ajouter-produits.component.html',
  styleUrls: ['./ajouter-produits.component.scss']
})
export class AjouterProduitsComponent implements OnInit {
  validationform: FormGroup; // bootstrap validation form

  // Form submition
  submitted: boolean;
  produit : Produit;
  categories : Categorie[];
  categorie : Categorie;
  image:string[] = [];
  
  constructor(
    public magasinService: MagasinService,
    public formBuilder: FormBuilder,
    private modalService: NgbModal,
    private produitService : ProduitsService,
    private categorieService: CategorieService,
    private redirectService : RedirectService,
    ) { }
  // bread crumb items
  breadCrumbItems: Array<{}>;
  router :Router;


  ngOnInit() 
  {
    this.redirectService.AuthetificationRedirect();
    this.redirectService.MagasinRedirect();
      this.breadCrumbItems = [{ label: 'Forms' }, { label: 'Form Validation', active: true }];
      /**
       * Bootstrap validation form data
       */
       this.validationform = this.formBuilder.group({
        designation: ['', [Validators.required]],
        quantite: ['', [Validators.required]],
        unite: ['', [Validators.required]],
        prix: ['', [Validators.required, ]],
        description: ['', [Validators.required, ]],
        categorie : ['', [Validators.required, ]],

      });
      this.Getcategories();


  }
  successmsg() {
    Swal.fire({
      icon: 'success',
      title: 'Your work has been saved',
      showConfirmButton: false,
      timer: 1500
    });
  }

    /**
   * Bootsrap validation form submit method
   */
 validSubmit() {
      this.submitted = true;
      console.log(this.form.categorie.value);
      if (this.validationform.invalid)
       {
        return;
      } 
      else
       {
         this.categorie={
           id:this.form.categorie.value,
           nom:null,
         }

         this.produit={
           id:null,
           designation:this.form.designation.value,
           unite : this.form.unite.value,
           quantite : this.form.quantite.value,
           description : this.form.description.value,
           prix:this.form.prix.value,
           magasin:this.magasinService.magasin,
           fournisseur:null,
           categorie: this.categorie,
           image1:this.image[0],
           image2:this.image[1],
           image3:this.image[2],
           image4:this.image[3],
         }
         this.produitService.Ajouter(this.produit).subscribe(response =>
          {
           if(response != null)
           {
            this.successmsg();
           }

         })


      }
    }
   /**
   * Returns form
   */
  get form() {
    return this.validationform.controls;
  }
       /**
   * save the contacts data
   */
  saveData() 
  {
    if (this.validationform.valid) {
      this.modalService.dismissAll();
    }
    this.submitted = true;
  }
  Getcategories()
  {
    this.categorieService.FindAll().subscribe(respond => 
    {
      this.categories=respond;
    })

  }
  OnFileChange(event)
  {
    this.image.push(event[0].dataURL);
    console.log(this.image);
    }
}
