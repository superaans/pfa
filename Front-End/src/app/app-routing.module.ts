import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HorizontalComponent } from './layouts/horizontal/horizontal.component';

import { LayoutComponent } from './layouts/layout/layout.component';
import { VerticalComponent } from './layouts/vertical/vertical.component';


const routes: Routes = [
  { path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule) },
  { path: '', component: HorizontalComponent, loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)},
  { path: 'dashboard', component: LayoutComponent, loadChildren: () => import('./administration/administration.module').then(m => m.AdministrationModule)},

  
 
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
  