export const environment = {
  production: true,
  defaultauth: 'fackbackend',
  apiURL: 'http://localhost:8080',
  firebaseConfig: {
    apiKey: '',
    authDomain: '',
    databaseURL: '',
    projectId: '',
    storageBucket: '',
    messagingSenderId: '',
    appId: '',
    measurementId: ''
  }
};
