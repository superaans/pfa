import { Gerant } from "./Gerant";

export interface Fournisseur
{
    id: number;
    nom :string;
    adresse: string;
    telephone: String;
    ville : String;
    gerant:Gerant;
}