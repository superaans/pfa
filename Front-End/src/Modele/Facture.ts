import { Fournisseur } from "./Fournisseur";
import { Magasin } from "./magasin";
import { Produit } from "./Produit";

export interface Facture{

    id:number;
    dateCreation : Date;
    prix : number;
    quantite : number;
    magasin : Magasin;
    produit : Produit;
    fournisseur : Fournisseur;
    }