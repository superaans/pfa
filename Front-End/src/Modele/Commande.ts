import { Gerant } from "./Gerant";
import { Magasin } from "./magasin";
import { Produit } from "./Produit";

export interface Commande{

id:number;
date_creation : Date;
montant : number;
etat : string;
magasin : Magasin;
produit : Produit;
gerant : Gerant;
qte : number;

}