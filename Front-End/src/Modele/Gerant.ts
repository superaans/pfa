export interface Gerant{
    id: number;
    nom :string;
    email: string;
    dateNaissance : Date;
    password: String;
    telephone: String;
    ville : String;
    type : boolean;
}