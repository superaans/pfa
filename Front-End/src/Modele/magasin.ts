import { Gerant } from "./Gerant";
import { Produit } from "./Produit";

export interface Magasin {
    id: number;
    nom :string;
    logo: string;
    cf : number;
    gerant:Gerant;
    produits:Produit[];
}
