import { Categorie } from "./Categories";
import { Fournisseur } from "./Fournisseur";
import { Magasin } from "./magasin";

export interface Produit 
{
    id: number;
    unite :number;
    designation:string;
    quantite: number;
    prix : number;
    magasin : Magasin;
    fournisseur : Fournisseur;
    description:string;
    categorie: Categorie;
    image1 : string;
    image2 : string;
    image3 : string;
    image4 : string;
}