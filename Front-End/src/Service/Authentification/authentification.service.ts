import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

import { Gerant } from 'src/Modele/Gerant';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

    private ApiUrul = environment.apiURL + "/authentification"
    constructor(private http: HttpClient) { }
  
    public Login(email : String , password : String): Observable<Gerant>{
      const headers = new HttpHeaders({
        "email" : email.toString(),
        'password' : password.toString(),
      })
      return this.http.get<Gerant>(this.ApiUrul , {headers : headers} )
    }
  
    public Inscription(gerant : Gerant): Observable<Gerant>{
      return this.http.post<Gerant>(this.ApiUrul  , gerant)
    }
  
    public Modifier(gerant : Gerant):Observable<Gerant>{
      return this.http.put<Gerant>(this.ApiUrul  , gerant)
    }
  }
