import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Categorie } from 'src/Modele/Categories';

@Injectable({
  providedIn: 'root'
})
export class CategorieService {

  private ApiUrul = environment.apiURL + '/Categorie'
  constructor(private http: HttpClient) 
  { }
  
  public FindAll():Observable<Categorie[]>
  {
    return this.http.get<Categorie[]>(this.ApiUrul )
  }
}
