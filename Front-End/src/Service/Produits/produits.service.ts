import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Produit } from 'src/Modele/Produit';

@Injectable({
  providedIn: 'root'
})
export class ProduitsService {

  private ApiUrul = environment.apiURL +  '/Produits'
  constructor(private http: HttpClient) { }
  public Ajouter(produit : Produit): Observable<Produit>
  {
    return this.http.post<Produit>(this.ApiUrul , produit)
  }

  public FindAll(id : number):Observable<Produit[]>
  {
    const headers = new HttpHeaders({
      "MagasinId" : id.toString()
    })
    return this.http.get<Produit[]>(this.ApiUrul  , {headers : headers} )
  }
  public delete(id : number): Observable<Produit> 
  {
    const headers = new HttpHeaders({
      "id" : id.toString()
    })
    return this.http.delete<Produit>(this.ApiUrul , {headers:headers});
  }
  public Update(produit : Produit): Observable<Produit>
  {
    return this.http.put<Produit>(this.ApiUrul , produit)
  }

  public getAll():Observable<Produit[]>
  {
    return this.http.get<Produit[]>(this.ApiUrul + '/tfo')
  }

  public getdetail(id : string):Observable<Produit>
  {
   
    return this.http.get<Produit>(this.ApiUrul + "/"+id)
  }
}
