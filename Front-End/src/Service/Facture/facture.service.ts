import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Facture } from 'src/Modele/Facture';

@Injectable({
  providedIn: 'root'
})
export class FactureService {
  private ApiUrul = environment.apiURL +  '/facture'
  constructor(private http: HttpClient) { }

  public Ajouter(facture : Facture): Observable<Facture>
  {
    return this.http.post<Facture>(this.ApiUrul , facture)
  }

  public findAll(magasinId : number): Observable<Facture[]>
  {
    const headers = new HttpHeaders({
      "magasinId" : magasinId.toString(),
    })
    return this.http.get<Facture[]>(this.ApiUrul , {headers : headers})
  }

}
