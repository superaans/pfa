import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MailService {

  private ApiUrul = environment.apiURL +  '/mail'
  constructor(private http: HttpClient) { }

  public sendMail(email : string , code : string):Observable<string>{
    const headers = new HttpHeaders({
      "email" : email,
      "code" : code
    })
    return this.http.get<string>(this.ApiUrul  , {headers : headers} )
  }
}
