import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Fournisseur } from 'src/Modele/Fournisseur';
import { Gerant } from 'src/Modele/Gerant';

@Injectable({
  providedIn: 'root'
})
export class FournisseurService {

  private ApiUrul = environment.apiURL + '/Fournisseurs'
  constructor(private http: HttpClient) { }
  public Ajouter(fournisseur : Fournisseur): Observable<Fournisseur>
  {
    return this.http.post<Fournisseur>(this.ApiUrul  , fournisseur)
  }

  public FindAll(gerant : Gerant):Observable<Fournisseur[]>
  {
    const headers = new HttpHeaders({
      "id" : gerant.id.toString(),
    })
    return this.http.get<Fournisseur[]>(this.ApiUrul , {headers:headers})
  }

  public delete(id : number): Observable<Fournisseur> 
  {
    const headers = new HttpHeaders({
      "id" : id.toString(),
    })
    return this.http.delete<Fournisseur>(this.ApiUrul , {headers : headers});
  }
  public Update(fournisseur : Fournisseur): Observable<Fournisseur>
  {
    return this.http.put<Fournisseur>(this.ApiUrul  , fournisseur)
  }
}
