import { HttpClient, HttpHeaders } from "@angular/common/http"
import { Injectable } from "@angular/core"
import { Observable } from "rxjs"
import { distinctUntilChanged } from "rxjs/operators"
import { environment } from "src/environments/environment.prod"
import { ImageModel } from "src/Modele/ImageModele"
import { Magasin } from "src/Modele/magasin"


@Injectable({
  providedIn: 'root'
})
export class MagasinService {
  private ApiUrul = environment.apiURL + "/magasins";

  public magasin:Magasin;

  constructor(private http: HttpClient) {
    if(localStorage.getItem('gerant')!=null){
      this.magasin = JSON.parse(localStorage.getItem('magasin'))
    }
   }

  public Liste(id : Number): Observable<Magasin[]>{
    const headers = new HttpHeaders({
      "id" : id.toString()
    })
    return this.http.get<Magasin[]>(this.ApiUrul, {headers : headers} )
  }

  public Ajouter(magasin:Magasin): Observable<Magasin>{
    return this.http.post<Magasin>(this.ApiUrul  , magasin)
  }
  
  public upload(image:FormData): Observable<ImageModel>{
    return this.http.post<ImageModel>(this.ApiUrul + '/upload/logo' , image)
  }
  
}
