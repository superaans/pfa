import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Gerant } from 'src/Modele/Gerant';
import { GerantService } from '../Gerant/gerant.service';
import { MagasinService } from '../Magasin/magasin.service';

@Injectable({
  providedIn: 'root'
})
export class RedirectService {

  constructor(
    private router : Router,
    private gerantService : GerantService,
    private magasinService: MagasinService,
    ) { }

  public AuthetificationRedirect(){
    const url : string  = this.router.url;
    localStorage.setItem("url" , url)
    if(this.gerantService.gerant == null){
      this.router.navigate(["/account/login"]);
    }else if(this.gerantService.gerant.type == false){
      this.router.navigate(["/account/verify"]);
    }
  }

  public MagasinRedirect(){
    if(this.magasinService.magasin == null){
      this.router.navigate(["/dashboard/magasin" , {"redirect"  : true} ] );
    }

  }
}
