import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Commande } from 'src/Modele/Commande';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  private ApiUrul = environment.apiURL +  '/commande'
  constructor(private http: HttpClient) { }

  public Ajouter(commandes : Commande[]): Observable<Commande[]>
  {
    return this.http.post<Commande[]>(this.ApiUrul , commandes)
  }

  public findAll(magasinId : number): Observable<Commande[]>
  {
    const headers = new HttpHeaders({
      "magasinId" : magasinId.toString(),
    })
    return this.http.get<Commande[]>(this.ApiUrul , {headers : headers})
  }
}
