import { Injectable } from '@angular/core';
import { Gerant } from 'src/Modele/Gerant';
import { AuthentificationService } from '../Authentification/authentification.service';

@Injectable({
  providedIn: 'root'
})

export class GerantService {
  constructor(authetificationService : AuthentificationService) {
    if(localStorage.getItem('gerant')!=null){
      this.gerant = JSON.parse(localStorage.getItem('gerant'))
      authetificationService.Login(this.gerant.email , this.gerant.password).subscribe(
        respond => {
          if(respond!= null){
            this.gerant = respond;
          }
          else{
            this.gerant = null;
          } 
        }
      )
    }
  }
  public gerant: Gerant;

  
}
